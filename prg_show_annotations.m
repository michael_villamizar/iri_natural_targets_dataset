%% IRI Natural Targets Dataset
% 
% Description:
%   This dataset contains five image sequences of natural targets in outdoors. In addition, 
%   the dataset includes handmade annotations of the different visual targets.
%   If anyone makes use of this dataset to conduct experiments and publish new articles, We 
%   kindly encourage to cite the below reference.
%
% Reference:
%  Fast Online Learning and Detection of Natural Landmarks for Autonomous Aerial Robots
%  M. Villamizar, A. Sanfeliu and F. Moreno-Noguer
%  International Conference on Robotics and Automation (ICRA), 2014
%
% Contact:
%   Michael Villamizar
%   mvillami-at-iri.upc.edu
%   Institut de Robòtica i Informática Industrial CSIC-UPC
%   Barcelona -  Spain
%

%% Main function
function prg_show_annotations()
clc,close all,clear all

% message
fun_messages('IRI Natural Targets Dataset','presentation');
fun_messages('IRI Natural Targets Dataset','title');

% parameters
imgPath = './scene2/images/';       % image file path
annPath = './scene2/annotations/';  % annotation file path
numMax = 200;  % num. max. images
lw = 4;    % line width
fs = 10;   % font size
ps = 1.0;  % pause time
step = 1;  % image step

% image and annotation files
imgFiles = dir([imgPath,'*.png']);
annFiles = dir([annPath,'*.mat']);

% num. images
numImgs = min(numMax,size(imgFiles,1));

% message
fun_messages('images:','process');
fun_messages(sprintf('num. images -> %d',numImgs),'information');

% images
for iterImg = 1:step:numImgs
 
    % file names
    annName = annFiles(iterImg).name;
    imgName = imgFiles(iterImg).name;
    
    % messages
    fun_messages(sprintf('img -> %d/%d : %s',iterImg,numImgs,imgName),'information');
    
    % show image
    img = imread([imgPath,imgName]);
    imshow(img),title(sprintf('%s',imgName),'fontsize',fs),hold on;
    
    % annotation data
    data = fun_load(annPath,annName);
    
    % show annotations 
    for iterObj = 1:data.numObjects
        
        % bounding box
        box = data.object{iterObj}.bbox;
        
        % orientation
        try 
            % dataset orientation
            angle = (pi/180)*data.object{iterObj}.angle;
        catch
            % default orientation
            angle = 0;
        end
        
        % show bounding box
        %rectangle('position',[box(1:2),box(3:4)-box(1:2)],'edgecolor','b','linewidth',lw);  % box
        rectangle('position',[box(1:2),box(3:4)-box(1:2)],'Curvature',[1,1],'edgecolor','r','linewidth',lw);  % cicle
        
        % show orientation -line-
        cx = (box(1)+box(3))/2;  % center point -x-
        cy = (box(2)+box(4))/2;  % center point -y-
        rd = (box(3)-box(1))/2;   % radius
        px = cx + rd*cos(angle-pi/2);  % end point -x-
        py = cy + rd*sin(angle-pi/2);  % end point -y-
        line([cx,px],[cy,py],'linewidth',lw,'color','r');
        
    end
    
    % pause
    pause(ps);
    close;
 
end

%message
fun_messages('end','title');

end

%% messages
% This function prints a specific message on the command window
function fun_messages(text,message)
if (nargin~=2), error('incorrect input parameters'); end

% types of messages
switch (message)
    case 'presentation'
        fprintf('****************************************************\n');
        fprintf(' %s\n',text);
        fprintf('****************************************************\n');
        fprintf(' Michael Villamizar\n mvillami@iri.upc.edu\n');
        fprintf(' http://www.iri.upc.edu/people/mvillami/\n');
        fprintf(' Institut de Robòtica i Informàtica Industrial CSIC-UPC\n');
        fprintf(' c. Llorens i Artigas 4-6\n 08028 - Barcelona - Spain\n 2014\n');
        fprintf('****************************************************\n\n');
    case 'title'
        fprintf('****************************************************\n');
        fprintf('%s\n',text);
        fprintf('****************************************************\n');
    case 'process'
        fprintf('-> %s\n',text);
    case 'information'
        fprintf('->     %s\n',text);
    case 'warning'
        fprintf('-> %s !!!\n',text);
    case 'error'
        fprintf(':$ ERROR : %s\n',text);
        error('program error');
end
end

%% load
function output = fun_load(path,name)

% load
data = load([path,name]);
data = data.data;

% output
output = data;
end