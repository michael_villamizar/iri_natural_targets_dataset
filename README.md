# IRI Natural Targets Dataset
 
## Description:
This dataset contains five image sequences of natural targets in outdoors. In
addition, the dataset includes handmade annotations of the different visual
targets. If anyone makes use of this dataset to conduct experiments and publish
new articles, We kindly encourage to cite the below reference.

## Reference:
Fast Online Learning and Detection of Natural Landmarks for Autonomous Aerial
Robots M. Villamizar, A. Sanfeliu and F. Moreno-Noguer International Conference
on Robotics and Automation (ICRA), 2014

## Code:
The Matlab function "prg_show_annotations" opens the dataset images and shows
the visual target annotations -bounding box and orientation-.

## Images:
The dataset has five image sequences acquired in outdoors, and where the visual
target is subject to in-plane and out-plane rotations, as well as lighting
changes. The image sequences are:

* bench -> 120 images
* ground1 -> 150 images
* ground2 -> 109 images
* scene1 -> 109 images
* scene2 -> 123 images

## Contact:
Michael Villamizar

mvillami-at-iri.upc.edu

Institut de Robòtica i Informática Industrial CSIC-UPC

Barcelona -  Spain
